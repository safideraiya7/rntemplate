import {
  SHOW_LOADER,
  SET_TOKEN,
  SET_USER_INFO,
} from '../types';


export const setUserInfo = value => {
  return async dispatch => {
    dispatch({
      type: SET_USER_INFO,
      payload: value,
    });
  };
};


export const setToken = value => {
  return async dispatch => {
    dispatch({
      type: SET_TOKEN,
      payload: value,
    });
  };
};


export const setLoader = value => {
  return async dispatch => {
    dispatch({
      type: SHOW_LOADER,
      payload: value,
    });
  };
};




