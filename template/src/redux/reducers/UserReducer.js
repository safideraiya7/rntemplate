import {
    SET_USER_INFO,
    SET_TOKEN,
} from '../types';

const INITIAL_STATE = {
    token: '',
    userInfo: '',
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_TOKEN:
            return {
                ...state,
                token: action.payload,
            };

        case SET_USER_INFO:
            return {
                ...state,
                userInfo: action.payload,
            };
        default:
            return state;
    }
}
