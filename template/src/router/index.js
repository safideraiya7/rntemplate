import React from 'react';

import Routes from './routes';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from 'src/screens/home/HomeScreen';
import LoginScreen from 'src/screens/login/LoginScreen';
import SplashScreen from 'src/screens/splash/SplashScreen';

const Stack = createNativeStackNavigator();

const AppNavigator = props => {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none" initialRouteName={Routes.SplashScreen}>
        <Stack.Screen
          options={{headerShown: false}}
          name={Routes.SplashScreen}
          component={SplashScreen}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name={Routes.LoginScreen}
          component={LoginScreen}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name={Routes.HomeScreen}
          component={HomeScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
