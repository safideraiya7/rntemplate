import React from 'react';
import {StyleSheet, Text, Button, View, TouchableOpacity} from 'react-native';
import styles from './styles';
import {setLoader, setToken} from 'src/redux/actions/CommanActions';
import {connect} from 'react-redux';
import Routes from 'src/router/routes';

const HomeScreen = ({setToken, navigation}) => {
  const clearAsyncStorage = async () => {
    setToken('');
    navigation.replace(Routes.LoginScreen);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={clearAsyncStorage}
        activeOpacity={0.8}
        style={styles.bottomContainer}>
        <Text style={styles.buttonText}>{'HOME SCREEN'}</Text>
      </TouchableOpacity>
    </View>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    setToken: params => dispatch(setToken(params)),
    setLoaderAction: params => dispatch(setLoader(params)),
  };
};

export default connect(null, mapDispatchToProps)(HomeScreen);
