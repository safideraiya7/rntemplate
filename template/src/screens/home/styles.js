import {StyleSheet} from "react-native";
import Colors from "src/utills/Colors";

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: Colors.white,
    },
    userTabContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'baseline',
        padding:20
    },
    DoorKeyContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    button: {
        width: 300,
        height: 50,
        backgroundColor: Colors.grey,
        borderRadius: 10,
        marginVertical: 10,
        paddingVertical: 14,
    },
    message: {
        fontSize: 20,
        fontWeight: '500',
        marginVertical: 15,
    },
    bottomContainer:{
        height:70,
        width:'100%',
        backgroundColor:Colors.black,
        alignItems:'center',
        justifyContent:'center',
    },
    buttonText:{
        fontSize:20,
        fontWeight:'bold',
        color: Colors.grey,
    }
});

export default styles;
