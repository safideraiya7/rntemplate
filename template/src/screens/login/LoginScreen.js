import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import styles from './styles';

import Strings from 'src/utills/Strings';
import {setLoader, setToken} from 'src/redux/actions/CommanActions';

const LoginScreen = ({setToken, navigation, setLoaderAction}) => {
  return (
    <View style={styles.container}>
      <View style={styles.form}>
        <Text style={styles.buttonText}>{Strings.signIn}</Text>
      </View>
    </View>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    setToken: params => dispatch(setToken(params)),
    setLoaderAction: params => dispatch(setLoader(params)),
  };
};

export default connect(null, mapDispatchToProps)(LoginScreen);
