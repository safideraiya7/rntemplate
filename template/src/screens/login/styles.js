import {StyleSheet} from 'react-native';
import Colors from 'src/utills/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  form: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 60,
  },
  input: {
    width: 300,
    height: 50,
    backgroundColor: Colors.grey,
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: Colors.black,
    marginVertical: 5,
  },
  button: {
    width: 300,
    height: 50,
    backgroundColor: Colors.green,
    borderRadius: 15,
    marginVertical: 10,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 20,
    fontWeight: '500',
    color: Colors.black,
    textAlign: 'center',
  },
  inputError: {
    fontSize: 16,
    color: Colors.red,
    marginTop: 8,
  },
});
export default styles;
