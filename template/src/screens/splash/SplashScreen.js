import React, {useState, useEffect} from 'react';
import {Text, View} from 'react-native';
import styles from './styles';
import {setLoader} from 'src/redux/actions/CommanActions';
import {connect} from 'react-redux';
import Routes from 'src/router/routes';

const SplashScreen = ({userToken, navigation}) => {
  useEffect(() => {
    if (userToken) {
      setTimeout(() => {
        navigation.replace(Routes.HomeScreen);
      }, 3000);
    } else {
      setTimeout(() => {
        navigation.replace(Routes.LoginScreen);
      }, 3000);
    }
  }, []);

  return (
    <View style={styles.container}>
      <Text>{'Splash'}</Text>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    userToken: state?.UserReducer?.token ? state.UserReducer.token : '',
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setLoaderAction: params => dispatch(setLoader(params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
