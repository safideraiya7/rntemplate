import {StyleSheet} from "react-native";
import Colors from "src/utills/Colors";

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: Colors.white,
        justifyContent:'center',
        alignItems:'center',
    },
});

export default styles;
