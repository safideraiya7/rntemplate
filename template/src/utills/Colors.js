const Colors = {
  black: '#000',
  white: '#fff',
  green: 'rgb(60, 179, 120)',
  grey: 'rgb(220,220,220)',
  red: 'red',
  black40: 'rgba(0,0,0,0.4)',
};

export default Colors;
