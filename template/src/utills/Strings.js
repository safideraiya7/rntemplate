const Strings = {
  AppName: '',
  emailPlaceholder: 'Email or Username',
  passwordPlaceholder: 'Password',
  signIn: 'Sign In',
  username: 'Username',
  logout: 'LOGOUT',
};

export default Strings;
